#!/bin/bash

# Remember to create the auth.json file. Use the file auth-example.json as a template.

printhelp () {
  echo Usage: ./tmitocar.sh -i inputFile [-o outputFormat] [-l label] [-a authFile] [-L language] [-s]
  echo This script currently supports txt and pdf files
  echo Supported languages are en and de
  echo Possible output formats are: png, pdf, xls, csv, svg, dot, txt, json
  echo -s silentMode - do not wait for user input
}

if (($# == 0))
then
    echo "No arguments provieded!"
    printhelp
    exit 1;
fi

while getopts hsi:o:l:a:L: options ; do
 case "${options}" in
 l) label=${OPTARG};;
 o) format=${OPTARG};;
 i)
    if [ -f $OPTARG ] ; then
      inputfile=${OPTARG}
    else
      echo "Specified file does not exist!"
      exit 1 ;
    fi
    ;;
 a) authfile=${OPTARG} ;;
 L) language=${OPTARG};;
 h) printhelp; exit ;;
 s) silentMode=true;;
 :)
    echo "Error: -${OPTARG} requires an argument."
    exit 1
    ;;
 *) exit;;
 \?) echo "Unknown option: -$OPTARG" >&2; exit 1 ;;
 esac
done

silentMode=${silentMode:-false}
label=${label:-test}
format=${format:-png}
language=${language:-de}

if [ -z $inputfile ] ; then
  echo "Option -i is required!"
  exit 1 ;
fi

if [ -z $authfile ] ; then
  echo "Trying to use default auth file: auth.json"
  authfile=auth.json
fi

if ! [ -f $authfile ] ; then
 echo "Auth file does not exist!"
 exit 1 ;
fi

date=$(date +'%Y%m%d')
authpkey=$(cat $authfile | jq -r '.pkey')
authukey=$(cat $authfile | jq -r '.ukey')
pkey=`echo -n "$authpkey$date" | sha384sum | awk '{ print $1 }'`

rm -rf tmp
mkdir tmp

filetype=$(file -i "$inputfile" | cut -d' ' -f2)
ending=".pdf"
if [ "$filetype" = "text/plain;" ]
then
    ending=".txt"
    cp $inputfile tmp
elif [ "$filetype" = "application/pdf;" ]
then
    docsplit text $inputfile --language deu -o tmp
    cat tmp/*.txt > ${inputfile%$ending}.txt
else
  echo "Input filetype not supported!"
  exit 1;
fi

echo "Using chain for a "$ending" file"
echo "Cleaning text ..."

# clean text
textcleaned1=`cat tmp/*.txt | tr -d '\n' | sed -e 's/"//g'`
textcleaned2=`echo $textcleaned1 | sed -e 's/“//g'`
textcleaned3=`echo $textcleaned2 | sed -e 's/„//g'`
textcleaned4=`echo $textcleaned3 | sed -e 's/&//g'`
textcleaned5=`echo $textcleaned4 | sed -e "s/'//g"`
textcleaned6=`echo $textcleaned5 | sed -e 's/[\x01-\x1F\x7F]//g'`

echo $textcleaned6 > ${inputfile%$ending}\-cleaned.txt
rm -rf tmp
echo "Text successfully cleaned and saved as "${inputfile%$ending}\-cleaned.txt
echo ""


echo "Sending text to T-Mitocar and awaiting model..."

curl -X POST -L -d "ukey=$authukey" -d "pkey=$pkey" -d "retrieval=receive" -d "lang=$language" -d "wordspec=none" -d "output=$format" -d "weighed=1" -d "discardafterseconds=1" -d "tmlabel=$label" -d "tmtext=$textcleaned6" https://tech4comp.paedagogik.uni-halle.de/cgi-bin/tech4comp-tmitocar.pl > ${inputfile%$ending}\-modell.$format

echo "Received model as "${inputfile%$ending}\-modell.$format

if [ "$silentMode" = false ] ; then
  read -n 1 -p "Do you want to open this file? [y,n]" doit
  case $doit in
    y|Y) xdg-open ${inputfile%$ending}\-modell.$format ;;
  esac
fi

#TODO use later to store&retrieve, not working yet:

#echo "Sending text to T-Mitocar..."
#curl -X POST -L -d "ukey=$authukey" -d "pkey=$pkey" -d "retrieval=transmit" -d "lang=$language" -d "wordspec=none" -d "output=store" -d "weighed=1" -d "discardafterseconds=30" -d "tmlabel=$label" -d "tmtext=$textcleaned6" https://tech4comp.paedagogik.uni-halle.de/cgi-bin/tech4comp-tmitocar.pl
#echo "Awaiting model creation..."
#sleep 5s
#echo "Downloading model..."
#curl -X POST -L -d "ukey=$authukey" -d "pkey=$pkey" -d "retrieval=receive" -d "lang=$language" -d "wordspec=none" -d "output=$format" -d "weighed=1" -d "discardafterseconds=30" -d "tmlabel=$label" -d "tmtext=" https://tech4comp.paedagogik.uni-halle.de/cgi-bin/tech4comp-tmitocar.pl > ${inputfile%$ending}\-modell.$format
#echo "Received model as "${inputfile%$ending}\-modell.$format


#http --form POST https://tech4comp.paedagogik.uni-halle.de/cgi-bin/tech4comp-tmitocar.pl ukey=$authukey pkey=$pkey retrieval=receive lang=$language wordspec=none output=png weighed=1 discardafterseconds=1 tmlabel="$label" tmtext="$textcleaned6" > $inputfile.png

#!/bin/bash

# Remember to create the auth.json file. Use the file auth-example.json as a template.

printhelp () {
  echo Usage: ./batchInput.sh -d directory [-o outputFormat] [-l label] [-a authFile] [-L language]
  echo This script sends all files inside the directory to t-mitocar. It supports txt and pdf files.
  echo Supported languages are en and de
  echo Possible output formats are: png, pdf, xls, csv, svg, dot, txt, json
}

if (($# == 0))
then
    echo "No arguments provieded!"
    printhelp
    exit 1;
fi

while getopts hd:o:l:a:L: options ; do
 case "${options}" in
 l) label=${OPTARG};;
 o) format=${OPTARG};;
 d)
    if [ -d $OPTARG ] ; then
      directory=${OPTARG}
    else
      echo "Specified directory does not exist!"
      exit 1 ;
    fi
    ;;
 a) authfile=${OPTARG};;
 L) language=${OPTARG};;
 h) printhelp; exit ;;
 :)
    echo "Error: -${OPTARG} requires an argument."
    exit 1
    ;;
 *) exit;;
 \?) echo "Unknown option: -$OPTARG" >&2; exit 1 ;;
 esac
done

label=${label:-test}
format=${format:-png}
language=${language:-de}

if [ -z $directory ] ; then
  echo "Option -d is required!"
  exit 1 ;
fi

for filename in $directory/*; do
  ending=${filename##*.}
  echo $filename
  if [ $ending == "txt" ] || [ $ending == "pdf" ] ; then
    ./tmitocar.sh -i $filename -o $format -l $label -a $authfile -L $language -s
  else
    echo Error: $filename is not a txt or pdf file. Skipping this file!
  fi
done

## Empfehlungen für möglichst gute Endresultate

* Scans sollten möglichst gerade und nicht gewölbt sein
  * leicht schiefe Scans kann das Programm ausgleichen
  * Wölbungen sind nicht auszugleichen
* Fußnoten, Kopfzeilen, Fußzeilen, etc. müssen manuell entfernt werden - bspw. per Abschneiden der PDFs/Bilder
  * Fußnoten, Kopfzeilen, Fußzeilen, etc. tauchen nicht/selten im T-Mitocar Modell auf, aber im extrahierten Text --> Gefahr für Verfälschung besteht
* Verweise im Text (bspw. per Fußnote) müssen manuell entfernt werden, da diese das Modell verfälschen
* Generierte PDFs (bspw. Export aus Word) funktionieren besser als mit OCR gescannte Texte (bspw. in Bezug auf Silbentrennung)
* Bei zweispaltigen Dokumenten wird die Reihenfolge des extrahierten Textes durcheinander gebracht. Bei Scans (inkl. OCR) desselben Textes hingegen nicht.
* Teils wird nur der erste Teil eines Textes zu einem grafischen Modell umgewandelt, ohne das ein Fehler erzeugt/ausgegeben wird. Modelle sind daher zu kontrollieren.

## Todos für Script

* "Stu-dierende", in cleaned.txt "Stu- dierende". Das bildet ein Muster. Bei zusammengesetzten Wörtern mit Bindestrich wird kein Leerzeichen eingefügt, z.B. "mentoring-Elemente" bleibt auch in cleaned.txt ohne Leerzeichen
* Teils wird nur der erste Teil eines Textes zu einem grafischen Modell umgewandelt, ohne das ein Fehler erzeugt/ausgegeben wird. Dies liegt an ...

## Todos aus Requirements Bazaar

Textbereinigung; Entfernen von:
- Sonderzeichen
  - Pfeile
  - Tilden
  - Asterisk
  - ~~Anführungszeichen - " “ „ '~~
  - ~~Und-Zeichen &~~
  - ~~Alle ASCII Control-characters~~
- Wide-Characters (z.B. Halbgevierte, französische Anführungszeichen, etc.), erlaubt sind nur ä,ö,ü,ß,é,à etc.
- Leerzeichen innerhalb von Worten
- ~~Leerzeilen innerhalb von Worten~~
- ~~Zeilenumbrüche innerhalb eines Satzes~~
- ~~Zeilenumbrüche innerhalb von Worten~~
- ~~Leerzeilen innerhalb eines Satzes~~
- Silbentrennung
- Meta-Daten (z.B. Verlag, Urheberrechtsbestimmungen etc.)
- Abbildungen, Tabellen, Bildern, Grafiken
- Beschriftungen jeder Art
- Inhalts- und Literaturverzeichnis
- Kopfzeilen
- Vorwort, Index, Glossar, Register
- Silbentrennungen
- Links
- Aufzählungszeichen innerhalb von Sätzen
- Fußnoten, Kommentare, Seitenzahlen
- Ganze englische Sätze in einem ansonsten deutschen Text

Weitere benötigte Überarbeitung:
- Überschriften, Unterüberschriften und Abschnittsüberschriften mit einem Punkt abschließen.

Beheben von Fehlern:
- Fehlende Leerzeichen zwischen Worten einfügen
- Fehlende Leerzeichen zwischen dem abschließenden Punkt am Satzende und dem ersten Wort des folgenden Satzes einfügen.
- Falsche Reihenfolge von kopierten Textabschnitten
- Linien im Text
- Unvollständige Sätze
- Falsch erkannte Buchstaben und Zeichen, wie z.B. 1 statt I.

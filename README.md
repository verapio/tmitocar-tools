# tmitocar-tools

**Usage**: First create the `auth.json` file by using `auth-example.json` as a template.

Type `./tmitocar.sh -h` to see the usage information.

A typical call to this script looks like: `./tmitocar.sh -i example.pdf`


## Requirements

* jq - parse json files
* docsplit - extract raw text from pdf files, optionally OCR and alignment correction

### docsplit

`sudo gem install docsplit`

docsplit won't install its dependencies when the mentioned command is used. Install at least these tools/libs (e.g. by using your package manager) additionally:
* tesseract
* tesseract-langpack-deu
* tesseract-langpack-eng
* tesseract-osd

### jq

`sudo zypper install jq`

## Parsr - Transforms PDF, Documents and Images into Enriched Structured Data

Type `./parsr.sh -h` to see the usage information.

A typical call to this script looks like: `./parsr.sh -i example.pdf`

The script requires as a prerequisite the installation of the program Parsr. Under the following link you can find the installation instructions.

<https://github.com/axa-group/Parsr#installation>

Under certain circumstances it is possible that more memory must be allocated to the Parsr API. This is possible with the following command:

`docker run -p 3001:3001 -e NODE_OPTIONS=--max_old_space_size=8192 axarev/parsr`


